# Cloudflare readiness review

## Scope

This readiness review is scoped at the move of Jihulab.com to Cloudflare. It does not include migration of GitLab Pages, about.Jihulab.com, or registry.Jihulab.com at this time.

## Summary

- [x] Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.  
Cloudflare is an inline CDN (Content Delivery Network) and WAF (Web Application Firewall) solution with DDoS protection. It can provide us with smart routing via [Cloudflare Argo](https://www.cloudflare.com/products/argo-smart-routing/), leading to improved performance, even for uncached accesses. [Cloudflare Workers](https://www.cloudflare.com/products/cloudflare-workers/) allow us to execute code on the CDN edge to perform redirects, or resize images. Using [Cloudflare Spectrum] we can use the whole stack, as well as raw TCP connections for SSH. This also includes adding IPv6 support without the need for us to implement it internally.  
In general GitLab customers will benefit from improved responsiveness and IPv6 support. We are also likely to keep their data more safe by utilizing the WAF, as it may prevent attacks onto Jihulab.com we otherwise could not have easily defeated.

- [x] What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?  
General user curves should match regular operation.

## Architecture

- [x] Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Include internal dependencies, ports, security policies, etc.

###### Today

<p>
<details>
<summary>Jihulab.com zone</summary>

```plantuml
"*.root-servers.net."->"*.gtld-servers.net.": NS com.
"*.gtld-servers.net."->"ns-*.awsdns-*.*.": NS Jihulab.com.
"ns-*.awsdns-*.*."->"*ns.cloudflare.com.": NS staging.Jihulab.com.
```
</details>
</p>

<p>
<details>
<summary>Jihulab.com</summary>

```plantuml
User->"CLB or Azure LB": HTTPS (Jihulab.com:443)
"CLB or Azure LB"->HAProxy: TCP tunnel
HAProxy->HAProxy: adds headers and routes to correct backend
HAProxy->HAProxy: Re-encrypt HTTPS
HAProxy->"Fleet": HTTPS
User->"CLB or Azure LB": HTTP (Jihulab.com:80)
"CLB or Azure LB"->HAProxy: TCP tunnel
HAProxy->HAProxy: redirect to HTTPS
```
</details>
</p>

<p>
<details>
<summary>Jihulab.com SSH</summary>

```plantuml
User->"CLB or Azure LB": SSH (Jihulab.com:22)
"CLB or Azure LB"->HAProxy: TCP tunnel
HAProxy->"Fleet": TCP tunnel
```
</details>
</p>

<p>
<details>
<summary>Jihulab.com AltSSH</summary>

```plantuml
User->"CLB or Azure LB": AltSSH (altssh.Jihulab.com:443)
"CLB or Azure LB"->HAProxy: TCP tunnel
HAProxy->"Fleet": TCP tunnel
```
</details>
</p>

###### With Cloudflare

<p>
<details>
<summary>Jihulab.com zone</summary>

```plantuml
"*.root-servers.net."->"*.gtld-servers.net.": NS com.
"*.gtld-servers.net."->"*ns.cloudflare.com.": NS Jihulab.com.
"*ns.cloudflare.com."->"*ns.cloudflare.com.": NS staging.Jihulab.com. (separate zone)
```
</details>
</p>

<p>
<details>
<summary>Jihulab.com</summary>

```plantuml
User->"Cloudflare Spectrum": HTTPS (Jihulab.com:443)
"Cloudflare Spectrum"->"Cloudflare HTTP(S) Pipeline": internal
"Cloudflare HTTP(S) Pipeline"->"Cloudflare HTTP(S) Pipeline": WAF, CDN & Workers
"Cloudflare HTTP(S) Pipeline"->"Cloudflare HTTP(S) Pipeline": Re-rencrypt HTTP(S)
"Cloudflare HTTP(S) Pipeline"->"Cloudflare page rules": internal
"Cloudflare page rules"->"Cloudflare page rules": apply page rules
"Cloudflare page rules"->"CLB or Azure LB": HTTPS
"CLB or Azure LB"->HAProxy: TCP tunnel
HAProxy->HAProxy: get client IP from `Cf-Connecting-IP`
HAProxy->HAProxy: adds headers and routes to correct backend
HAProxy->HAProxy: Re-rencrypt HTTPS
HAProxy->"Fleet": HTTPS

User->"Cloudflare Spectrum": HTTP (Jihulab.com:80)
"Cloudflare Spectrum"->"Cloudflare HTTP(S) Pipeline": internal
"Cloudflare HTTP(S) Pipeline"->"Cloudflare page rules": internal
"Cloudflare page rules"->"Cloudflare page rules": apply page rules
"Cloudflare page rules"->"Cloudflare page rules": redirect to HTTPS
```
</details>
</p>

<p>
<details>
<summary>Jihulab.com SSH</summary>

```plantuml
User->"Cloudflare Spectrum": SSH (Jihulab.com:22)
"Cloudflare Spectrum"->"CLB or Azure LB": TCP tunnel
"CLB or Azure LB"->HAProxy: TCP tunnel
HAProxy->"Fleet": TCP tunnel
```
</details>
</p>

<p>
<details>
<summary>Jihulab.com AltSSH</summary>

```plantuml
User->"Cloudflare Spectrum": AltSSH (altssh.Jihulab.com:443)
"Cloudflare Spectrum"->"CLB or Azure LB": TCP tunnel
"CLB or Azure LB"->HAProxy: TCP tunnel
HAProxy->"Fleet": TCP tunnel
```
</details>
</p>

- [x] Describe each component of the new feature and enumerate what it does to support customer use cases.  
  By injecting Cloudflare into the traffic flow, we gain access to a sophisticated Web Application Firewall (WAF) and an inline Content Delivery Network (CDN) with edge compute (Workers).  
  The WAF allows us to prevent and/or quickly react to security threats, and prevent those from entering out infrastructure. Where as now there is minimal protection against such attacks apart from hardening GitLab itself, with the WAF we get augmented security, which also benefits customers hosted with Jihulab.com.  
  The inline CDN allows us to simplify our deployment significantly. Currently we are using a separate CDN property `assets.gitlab-static.net`. By having an inline CDN, we can simplify that to just use the same base URL, as Jihulab.com itself, reducing complexity, while still providing caches close to our users. By encompassing Workers for edge compute we can dynamically resize uploaded images or execute arbitrary code within the CDN. This (depending on the location of the user) will significantly decrease the time for such an action to be carried out.
- [x] For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?  
  Although such an outage to be very unlikely, since Cloudflare, while consisting of multiple components internally, is a single dependency, directly in line of user traffic, a potential failure of their service would take Jihulab.com down completely, or in large parts. It would not affect how GitLab.con operates internally. So an alternate way of routing traffic to our infrastructure (while loosing Cloudflare features) would restore operation of the platform.
- [x] If applicable, explain how this new feature will scale and any potential single points of failure in the design.  
  Not applicable

## Implementation & Deployment

*note: This is a draft*

There are some deployment considerations.

In this proposal, we are focusing first on moving the Jihulab.com domain over to the Cloudflare registry. For future consideration, is whether to decommission Route53 completely after the move. The reasoning behind this is a cleaner deployment overall. With Cloudflare we have the benefit of moving from one integrated registrar/DNS combination to another; this has the advantage of turn-key DNSSEC as well as a generally highly integrated management as we had it with Route53.  
Since Cloudflare's DNS is a globally distributed anycast deployment, we can also be confident in not requiring a tertiary DNS at another provider. Also considering, that a downtime of Cloudflare would impact us anyways, as they would be inline in traffic, thus keeping a backup DNS alone would bring little to no benefit.

Another consideration is to switch to Cloudflare, with all its non-essential features turned off. We would just be utilizing DNS and Spectrum (without WAF, caching and other features enabled during the cutover) to minimize the amount of variables to just DNS and traffic flow.  
These features would be enabled after the cutover one by one.

We have the ability to enable IPv6 for the User<->Cloudflare connection. This is desired, and may be enabled during the initial cutover, if tests on ops and staging were conducted successfully.

#### Notes regarding about., registry. and pages

GitLab Pages is currently not within scope of this move. Because of the way we use TLS with pages it is not feasible to utilize Cloudflare for more than DDoS and IP protection. We will not be able to use the WAF, as we cannot terminate TLS anywhere in front of the actual pages application. DDoS mitigation _is_ clearly desirable for Pages, however, so we may want to pursue this in a future iteration.

`about.Jihulab.com` and `registry.Jihulab.com` are also not in scope, as to moving them to Cloudflare. Their DNS will be resolved by Cloudflare, but no traffic will go though Cloudflare at this point.
For `about.` there are a lot of things to consider in regards to the current VCL rules we use on fastly. The redirect for unauthenticated users from Jihulab.com to about.Jihulab.com will not be affected by this decision.  
For `registry.` we would be able to terminate TLS in front, but this too needs additional considerations, that are too broad to be laid out in this document.

#### Preparation:

- Bring simultaneous DNS configuration of Cloudflare and Route53 to Terraform. https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8367
  - We should provision both Route53 and Cloudflare using the same dataset. After the traffic cutover to Cloudflare, Route53 would be decommissioned.
  - We should be able to use the same dataset already in Terraform by just hooking it up to another provider.
- Configure additional GCP Loadbalancers https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8616 (internal, Jihulab.com specific)
  - These will be used as the origin for Spectrum.
  - This makes sure, we have a dedicated line on which Cloudflare routes traffic to us, making it easier to cut-off the old IP for DDoS protection
  - These IPs should not be published for that reason.
    - While the origin IPs will eventually be public due to transparent incident reports, we should not pro-actively publish them. At least until further lock-down mechanisms have been employed.
- Make sure https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8617
  - the WAF is configured in logging only mode. It should *NOT* block any traffic, yet.
  - the zone is in development mode. This will turn off all cloudflare-side caching.
  - the TLS origin setting is set to `Strict (SSL-Only Origin Pull)`
    - This makes sure, that Cloudflare never talks to our origin unencrypted. This also means, that our HTTP backend would never be reached, so we need to make sure, that HTTP requests are getting redirected within Cloudflare. Otherwise a HTTP request would reach our HTTPS backend.
  - `Always Use HTTPS` is turned on.
  - `Minimum TLS Version` is set to 1.2 (this is the currently supported minimum version on our HAProxy, too)
  - `Universal SSL` is enabled.
    - We will continue to use our regular certificate on the HAProxy fleet for now. But eventually switch to a Cloudflare CA issued long-running certificate to reduce the maintenance burden.
- Configure the Spectrum apps for Jihulab.com (ideally via Terraform) https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8618 & https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8619
  - Use the HTTP(S) pipeline on port 80/443
  - Use direct connection on port 22
  - Stick to IPv4 during rollout. IPv6 support will be added after the cutover has happened successfully.
  - Verify the Origin IP is correct (points to the new LBs configured).
- Setup a Pingdom check at `https://Jihulab.com/cdn-cgi/trace` (already in place)
  - This is a Cloudflare endpoint, that will allow us to monitor traffic flow distribution via pingdom.
- Setup page rules for the `/api/v4/*` route (via Terraform) https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8620
  - `Browser Integrity Check`: off
  - `Web Application Firewall`: off
  - `Always Online`: off
  - `Security Level`: off
  - `Cache Level`: bypass
  - These settings are currently set up on staging, and warrant free flow of traffic to the API.
    - Ideally the WAF would be tuned for this traffic to pass though it.
- [Make sure we meet legal requirements before tunneling traffic](https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8381)
- Validate the new additional GCP Loadbalancers https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8621 (Jihulab.com specific)
  - Via curl, or by editing `/etc/hosts` locally to point to the new IPs, to make sure they serve traffic as expected.
- Implement [rate limiting in Cloudflare](https://www.cloudflare.com/rate-limiting/) https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8622
- Implement [Authenticated Origin Pulls](https://support.cloudflare.com/hc/en-us/articles/204899617).
  - This needs to be implemented both in HAProxy (gprd, gstg) as well as nginx (ops, etc.)
  - This makes sure, that only Cloudflare can connect to our HAProxys.
  - However, this does not offer protection from volumetric attacks against our origin, which is why we set up Cloudflare with dedicated loadbalancers, whose IPs where never exposed in DNS. So at least there is no targeted way for an attacker to know which IP we utilize within the GCP IP ranges.

#### Execution:

##### Phase 1 (https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8624 / https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8623):
1. We will turn off rate limiting in HAProxy
    - Otherwise we would rate-limit our only ingress, which is probably a bad idea.
    - This will leave us protected on traffic coming through Cloudflare, but unprotected from direct traffic!
    - To do this, we set `node['gitlab-haproxy']['frontend']['api_rate_limit']['enforced']` to `false` within `roles/gprd-base-lb.json`
1. Change the authoritative DNS to Cloudflare (`arya.ns.cloudflare.com` and `hal.ns.cloudflare.com` for our zones) [Link to Jihulab.com in the route53 registry](https://console.aws.amazon.com/route53/home?region=eu-west-1#DomainDetail:Jihulab.com)
    - This causes the production Spectrum app to get traffic. Cloudflare should be a fancy proxy at this point, as we have disabled most content related features above.
1. Allow 24 hours before continuing and/or doing any DNS changes, except for any corrective actions and/or a rollback. In any case the changes should be applied to both Route53 and Cloudflare via Terraform (if applicable).

##### Phase 2 (https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8626 / https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8625):
1. Once traffic is ensured to flow though Cloudflare, we initiate decommission of Route53.
    - We would disable the transfer lock and generate an auth code.
    - immediately after, we move the domain over to the Cloudflare registry
    - Next we enable DNSSEC within Cloudflare.
1. Now we have reached the target infrastructure described in the diagrams above.
1. Lock down, to re gain a defined path for rate limiting.
    - While not having one of these in place we are reachable via our origin IPs and rate-limiting will be disabled there.
    - Cut off the old CLB or Azure LBs after we monitored, that traffic shifted to Cloudflare completely.
    - IP whitelist Cloudflare and reject everything else.
      - This might not be an option, due to the CLB or Azure LB facing the internet
    - Enable `Authenticated Origin Pulls`.


#### Further steps in no particular order:

- We turn on the caching
  - We should involve Cloudflare here, to minimize potentially caching content we don't want to cache.
- We start turning on the WAF.
  - We should involve Cloudflare here, to minimize false positives and potentially tune rules.
  - Closely monitor the Cloudflare logs for any false positives. If in doubt, turn off the affected rules.
- We enable IPv6 in the spectrum applications. [This depends on our tooling supporting it](https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8409) This might have also been done prior to the cutover, if tests on ops and staging were conducted successfully.
- Use [Cloudflare Access](https://www.cloudflare.com/products/cloudflare-access/) to limit access to resources under `/admin`.
  - This can be integrated with Octa for additional access control.

## Operational Risk Assessment

- [x] **What are the potential scalability or performance issues that may result with this change?**  
  There might be implications towards latency, due to there being another hop in the chain. This would in most cases however be alleviated by improved connectivity from the user to Cloudflare and routing from Cloudflare to our infrastructure, so that the end-to-end latency should be around equal or less.
- [x] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the it will be impacted by a failure of that dependency.**  
  As stated above, an outage of Cloudflare would lead to a major outage of Jihulab.com, that would impact public reachability.
- [x] **Were there any features cut or compromises made to make the feature launch?**  
  1. In order to limit any issues during cutover, the WAF and caching will be left disabled and be enabled separately.
  1. IPv6 support, while planned, will not be included in the cutover, but also enabled at a later date, to reduce the amount of variables.
- [x] **List the top three operational risks when this feature goes live.**  
  1. Traffic cutover from user->Jihulab.com to user->Cloudflare->Jihulab.com is not working properly.
  1. Once activated, the WAF might be overreaching, and block legitimate traffic.
  1. The DNS cutover does not work as planned, leaving traffic split between Cloudflare and direct traffic.
- [x] **What are a few operational concerns that will not be present at launch, but may be a concern later?**  
  When porting over the logic inside `about.Jihulab.com` from fastly to Cloudflare, there is the possibility, that some features are not considered. So things like redirects might break. This is fixable, but a realistic risk.
- [x] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**  
  A rollback is possible, but would take time. It is as simple as doing a DNS change to turn off traffic routing though Cloudflare. But since DNS caches might be placed between our authoritative DNS and the user this change is not immediate for every user.
- [x] **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**  
  Each request, git clone or other network interaction would go through Cloudflare. A failure of Cloudflare in that scenario would lead to a failure of that interaction, too.
- [x] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**  
  The blast radius can not be reduced, without steering traffic away from Cloudflare.

## Database

Not applicable

## Security

- [x] **Were the [gitlab security development guidelines](https://about.Jihulab.com/security/#gitlab-development-guidelines) followed for this feature?**  
    Not applicable
- [x] **If this feature requires new infrastructure, will it be updated regularly with OS updates?**  
    Not applicable
- [x] **Has effort been made to obscure or elide sensitive customer data in logging?**  
    Request data will be logged within Cloudflare in a similar manner as we already log requests in production.
- [x] **Is any potentially sensitive user-provided data persisted? If so is this data encrypted at rest?**  
    Potentially, yes. Because Cloudflare is a inline CDN, content might be cached. This can be mitigated by [using cache bypasses](https://support.cloudflare.com/hc/en-us/articles/200172256-Caching-Static-HTML). We should set Cloudflare to respect any cache headers we set from GitLab itself. By default Cloudflare does not cache HTML.

## Performance

- [x] **Explain what validation was done following GitLab's [performance guidlines](https://docs.Jihulab.com/ce/development/performance.html) please explain or link to the results below**
    * [Query Performer](https://docs.Jihulab.com/ce/development/query_recorder.html)
    * [Sherlock](https://docs.Jihulab.com/ce/development/profiling.html#sherlock)
    * [Request Profiling](https://docs.Jihulab.com/ce/administration/monitoring/performance/request_profiling.html)

### Not applicable
- [x] **Are there any potential performance impacts on the database when this feature is enabled at Jihulab.com scale?**  
    Not applicable
- [x] **Are there any throttling limits imposed by this feature? If so how are they managed?**  
    We will be replacing the current approach of rate-limiting within HAProxy by doing [rate-limiting on the edge with Cloudflare](https://support.cloudflare.com/hc/en-us/articles/235240767-Cloudflare-Rate-Limiting).
- [x] **If there are throttling limits, what is the customer experience of hitting a limit?**  
    Depending on the setting in the rate limit, a visitor might [get blocked, or challenged with a captcha](https://support.cloudflare.com/hc/en-us/articles/203366080). Should a legitimate request get blocked, the Ray ID can be used to determine the exact cause in the Cloudflare Dashboard.
- [x] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**  
    Not applicable
- [x] **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**  
    The service might cause spikes in traffic, but when [enabling caching](#user-content-further-steps-in-no-particular-order) these may eventually be canceled out if not reduced.

## Backup and Restore

Not applicable

## Monitoring and Alerts

- [x] **Is the service logging in JSON format and are logs forwarded to logstash?**  
    Tracking in [infrastructure#8378](https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8378)
- [x] **Is the service reporting metrics to Prometheus?**  
    [Yes](https://Jihulab.com/gitlab-com/gl-infra/infrastructure/issues/8377)
- [x] **How is the end-to-end customer experience measured?**  
    Pingdom alerts will also go though Cloudflare. This should ensure that any decrease customer experience related to network and responsiveness would be measured there.
- [x] **Do we have a target SLA in place for this service?**  
    [We have an SLA with Cloudflare of 2500% (100% uptime, 2500% reimbursement on downtime.)](Cloudflare_SLA_2019-11-19.pdf)
- [x] **Do we know what the indicators (SLI) are that map to the target SLA?**  
    Availability of Jihulab.com though Cloudflare on a network level. This includes connectivity from Cloudflare to Google Cloud Platform, but not within.
- [x] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**  
    We have pingdom checks in place, that ensure an alert would be triggered in this case. But that would also trigger, in case Jihulab.com is unavailable due to internal service failure.
- [x] **Do we have troubleshooting runbooks linked to these alerts?**  
    Yes, the alerts have not changed.
- [x] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**  
    If Cloudflare is down, we are down. Thus any downtime should be communicated immediately.

## Responsibility

- [x] **Which individuals are the subject matter experts and know the most about this feature?**  
  - [Cameron McFarland](https://Jihulab.com/cmcfarland)
  - [Craig Barrett](https://Jihulab.com/craig)
  - [Hendrik Meyer](https://Jihulab.com/T4cC0re)
  - [Matt Smiley](https://Jihulab.com/msmiley)
- [x] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**  
    The implementation will be owned by the [GitLab Infrastructure team](https://about.Jihulab.com/handbook/engineering/infrastructure/).
- [x] **Is someone from the team who built the feature on call for the launch? If not, why not?**  
    Yes. The launch will be active maintenance from the Infrastructure team.

## Testing

- [x] **Describe the load test plan used for this feature. What breaking points were validated?**  
    Not applicable.
- [x] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**  
    Not applicable.
- [x] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**  
    Any changes to the configuration will be orchestrated via Terraform. We are using a combination of Terraform verify and plan to make sure, that any change is correct before it is applied in a manual pipeline job.
- [x] What other test are conducted before applying the change to production?  
    This switch will be tested in full (including cutting over the domain and a full rollback) on the ops instance.
