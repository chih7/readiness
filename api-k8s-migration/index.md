[[_TOC_]]

# Migrating API Service to K8s

After [git https](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/228) and
[websockets](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/355), the API
service will be the third rails service being moved to K8s. This should result
in reduced deploy times and better resource usage while making use of the
official helm chart. Completing this work is one of the OKR's for the Delivery
team for Q2FY22: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112

This readiness review document is part of
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1463.


## Architecture

See https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/api#architecture.

## Performance

We currently (Mar 2021) serve between 4k to 5.5k API requests/s to
[workhorse](https://thanos-query.ops.gitlab.net/graph?g0.range_input=1w&g0.max_source_resolution=0s&g0.expr=avg_over_time(gitlab_component_ops%3Arate_5m%7Bcomponent%3D%22workhorse%22%2Cenv%3D%22gprd%22%2Cenvironment%3D%22gprd%22%2Cmonitor%3D%22global%22%2Cstage%3D%22main%22%2Ctype%3D%22api%22%7D%5B5m%5D)&g0.tab=0)
and 2k to 4k requests/s to
[puma](https://thanos-query.ops.gitlab.net/graph?g0.range_input=1w&g0.max_source_resolution=0s&g0.expr=avg_over_time(gitlab_component_ops%3Arate_5m%7Bcomponent%3D%22puma%22%2Cenv%3D%22gprd%22%2Cenvironment%3D%22gprd%22%2Cmonitor%3D%22global%22%2Cstage%3D%22main%22%2Ctype%3D%22api%22%7D%5B5m%5D)&g0.tab=0).

Performance of the API service mainly depends on these factors:

* amount of nodes (as of writing 36 in our main stage + 4 for the canary stage)
* node type (c2-standard-16)
* number of puma worker_processes per node (16)
* number of puma min (1) and max (4) threads

This results in 640 workers.

See https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1592 for a detailed
analysis.

**It is important to always have enough API capacity so that rolling deployments
or turning off canary is not affecting user experience.**

### K8s

In a K8s deployment we need to tune resource requests, autoscaling and
rollingUpdate settings to ensure API is able to meet it's SLOs and can react to
surges in traffic as well as to prevent frequent Pod eviction as starting an API
Pod is taking a long time. API Pods take approximately 1 minutes and 10 seconds
to start up.  Note that we delay the readiness probe 60 seconds, which
contributes to the vast majority of time we are waiting.  This is done
intentionally due to issues when Pods are reporting healthy, but may still be
starting up.  This is captured in issue:
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1294

We are focusing on the following configuration:

* hpa target values
* rollingUpdate strategy maxSurge
* minReplicas and maxReplicas
* resource requests and limits for CPU and memory

We will test those parameters by slowly shifting traffic from our VM fleet to
K8s and adapting if necessary. The API node pool is using the same machine type
as our current API VMs. For most of the above values, we'll start with our
defaults or our chart provided defaults.

* HPA Targets:
  * 1600m average Pod CPU utilization
  * 48 min Pods (per cluster)
  * 150 max Pods (per cluster)
  * Note the Canary stage, which runs on a single regional cluster, will use our
    webservice chart global configuration.  This matches a 1600m CPU average for
  scale, and sets the minimum Pod count to 5, and the maximum Pod count to 150
* Deployment Strategy will utilize the Kubernetes defaults
  * Default strategy is RollingUpdate
  * 25% max unavailable Pods
  * 25% max surge
  * Progress Deadline is 600 seconds
* Resource requests and limits is currently shared between all webservice
  deployments
  * See the `Performance` section of this document which contains further
    details of the proposed configuration

See https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1592 for details
into the investigation into these metrics targeted at the API deployment.

A few details on Pod counts; We currently run 36 API nodes on our main stage
with 16 puma workers each, for each worker we spawn between 1 and 4 threads.
This equates to running 576 workers across the fleet, and upwards of 2304
threads.  As we currently run 4 workers per Pod, this equates to minimally 48
Pods per zonal cluster.  With this we'll run 144 total Pods minimum.  A maximum
value of 150 per cluster was chosen for two reasons.  One, if we lose a cluster
for X reason, we'll have capacity to scale upwards.  Two, for growth and or
miscalculations.  Should Kubernetes need more resources to run our Pods, or we
are growing API traffic quickly, 150 will provide roughly 3X Pod capacity over
our initial estimates.

For the canary stage, we run 4 VM's, still with 16 workers, providing a total of
64 workers.  This fits nicely with our existing configuration.  When canary
starts taking all traffic in Kubernetes, we should see our autoscale settle with
roughly 16 Pods during peak load times.

## Migration Plan

### Current Virtual Machine configuration with HAProxy

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
  card "api" as A1
  card "canary_api" as A2
}
rectangle "api-XX VMs" as B {
  card "canary" as B1 {
      rectangle "nginx/workhorse/puma" as B11
  }
  card "main" as B2 {
      rectangle "nginx/workhorse/puma" as B21
   }
}

A1 --> B21 : main
A2 --> B11 : cny
```

The Canary stage for API is routed based on request path and additionally taking
a small percentage of random traffic and diverting it to the canary stage to
give it a more realistic traffic pattern.

#### Phase 1

The first phase will be to move a portion of our traffic to the canary namespace
in the Kubernetes cluster. The canary VMs will be replaced with the canary
namespace in the cluster. As canary traffic is too small we will not split it
into 3 zonal GKE clusters but rather serve it all from the regional GKE cluster.
This is the same method for which our git canary fleet is deployed.

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
      rectangle "api backend" as A1
      rectangle "canary_api backend" as A2
    }
    rectangle "api-XX VMs" as B {
      card "main" as B2 {
          rectangle "nginx/workhorse/puma" as B21
       }
    }
    rectangle "Production GKE" as C {
      card "gitlab-cny namespace" as C1 {
          card "webservice pod" as C11 {
            rectangle "puma" as C111
            rectangle "workhorse" as C112
          }
          rectangle "nginx ingress" as C12
       }

      card "gitlab namespace" as C2 {
          card "webservice pod" as C21 {
            rectangle "puma" as C211
            rectangle "workhorse" as C212
          }
          rectangle "nginx ingress" as C22
       }
    }

    A1 --> B21
    A2 --> C12
```

* Doing so will assist us in finding any new potential blockers and validate
  that the service is working as desired.

#### Phase 2

Once we are confident in canary working appropriately, we'll begin slowly
shifting traffic from our VMs into Kubernetes. We'll do this using weights on
HAProxy. This is the same procedure we've used in the past when migrating our
git services over. After this transition is complete, the VMs can be scaled
down.

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
      rectangle "api backend" as A1
      rectangle "canary_api backend" as A2
    }
    rectangle "api-XX VMs" as B {
      card "main" as B2 {
          rectangle "nginx/workhorse/puma" as B21
       }
    }
    rectangle "Production GKE" as C {
      card "gitlab-cny namespace" as C1 {
          card "webservice pod" as C11 {
            rectangle "puma" as C111
            rectangle "workhorse" as C112
          }
          rectangle "nginx ingress" as C12
       }

      card "gitlab namespace" as C2 {
          card "webservice pod" as C21 {
            rectangle "puma" as C211
            rectangle "workhorse" as C212
          }
          rectangle "nginx ingress" as C22
       }
    }

    A2 --> C12
    A1 --> C22 : 0-100%
    A1 --> B21 : 0-100%
```

### Network

The migration from Virtual Machines to GKE introduces additional network hops
due to the use of Kubernetes and removal of a local socket previously utilized
by workhorse on Virtual Machines.


#### Virtual Machine Network Topology

We currently run the `api-*` fleet on Virtual Machines that are directly
attached to HAProxy VMs. Recently, we configured HAProxy so that VMs in the same
availability zone are preferred which helped to reduce our cloud cost for
cross-zone network traffic.

```plantuml
skinparam roundcorner 20
skinparam shadowing false

cloud "Public Internet" as PI
rectangle "TCP LB :443" as TCP_LB

folder "us-east1-b" as F1B {

  rectangle "HAProxy" as HAProxyB
  storage "API VM fleet" as ApiB
}

folder "us-east1-c" as F1C  {
  rectangle "HAProxy" as HAProxyC
  storage "API VM fleet" as ApiC

}


folder "us-east1-d" as F1D {

  rectangle "HAProxy" as HAProxyD
  storage "API VM fleet" as ApiD

}

PI -- TCP_LB
TCP_LB -- HAProxyB
TCP_LB -- HAProxyC
TCP_LB -- HAProxyD
HAProxyB -- ApiB
HAProxyC -- ApiC
HAProxyD -- ApiD
```

Notes:

* Lines that cross availability zones are where we are billed at a higher rate
* In our current configuration there are a few points of cross-zone network traffic, it occurs between the `api-*` fleet and our stateful backends, Gitaly, Postgres, and Redis.


### GKE Network Topology for Webservice


```plantuml
skinparam roundcorner 20
skinparam shadowing false

cloud "Public Internet" as PI


rectangle "GitLab.com TCP LB :443" as TCP_LB

PI -- TCP_LB

folder "us-east1-b" as F1B {
  rectangle "HAProxy" as HAProxyB
  rectangle "Internal GKE TCP LB :443" as GKE_LBB
  node "nginx ingress" as NGINXB
  node "webservice pod" as WebB
}

folder "us-east1-c" as F1C  {
  rectangle "HAProxy" as HAProxyC
  rectangle "Internal GKE TCP LB :443" as GKE_LBC
  node "nginx ingress" as NGINXC
  node "webservice pod" as WebC
}


folder "us-east1-d" as F1D {
  rectangle "HAProxy" as HAProxyD
  rectangle "Internal GKE TCP LB :443" as GKE_LBD
  node "nginx ingress" as NGINXD
  node "webservice pod" as WebD
}

TCP_LB -- HAProxyB
TCP_LB -- HAProxyC
TCP_LB -- HAProxyD


HAProxyB -- GKE_LBB
HAProxyC -- GKE_LBC
HAProxyD -- GKE_LBD

GKE_LBB -- NGINXB
GKE_LBC -- NGINXC
GKE_LBD -- NGINXD

NGINXB -[#MidnightBlue]- WebB

NGINXC -[#MidnightBlue]- WebC

NGINXD -[#MidnightBlue]- WebD
```

Notes:

* In our Kubernetes infrastructure, the connection between NGINX and Webservice (workhorse/puma)
  changes from a local unix socket `
  unix:/var/opt/gitlab/gitlab-workhorse/socket` to a TCP connection
* The connection between NGINX and the Webservice pod is not encrypted
  (highlighted in blue)


## Operational Risk Assessment

## What are the internal and external dependencies of this service, are there SPOFs?

### Consul

Consul and Consul-DNS is used for database [service
discovery](https://docs.gitlab.com/ee/administration/database_load_balancing.html#service-discovery)
to fetch the list of replicas.  In the situation where Consul is completely
removed, the application will gracefully revert to only using the Primary
database.

For example, deleting the Consul service will result in these application
errors:

```
Service discovery encountered an error: No response from nameservers list
Sending event 1b60bcdb190144479175dda4a79bdd30 to Sentry
```

But the application will still function, though more pressure will be put on the primary DB.

Note that we are actively investigating an issue with Service Discovery:
https://gitlab.com/gitlab-org/gitlab/-/issues/271575.  It should be noted that
this is an issue that has been going on for quite awhile and will be further
evaluated when we first send traffic to canary to determine if this will remain a
blocker.

### Existing dependencies outside of the cluster

The following dependencies are dependencies of the API service, but are already
present for Sidekiq, git-https and websockets.

* Consul
* Gitaly
* HAProxy
* Patroni / PgBouncer
* Redis (persistent/cache/sidekiq)

## What are the potential scalability or performance issues that may result with this change?

### Scaling

There is a risk that we will not be able to adapt quickly enough to increases in
traffic, or sudden increases will cause saturation on the node. To mitigate this
we have:

* To start, we are slightly over-provisioning and setting a generous reserved
  capacity of pods
* Isolating this workload in its own node-pool will minimize impact to other
  services.
* The ability to tweak the Horizontal Pod Autoscaler configuration
* Tweaking the node pool instance types that this workload runs is also an option
* During the migration, we'll be carefully monitoring the metrics associated
  with this service and adjust our scaling configuration as necessary until we
  are taking 100% of the traffic in Kubernetes.
* Should we conveniently suffer a DDOS attack of sorts, we already have a couple
  of helpful items:
  * CloudFlare WAF
  * RackAttack
  * HAProxy Rate Limiting ACL rules
  * Along with the above, if we determine that an attack is negatively impacting
    us, this will undoubtedly be a high severity incident.  In this case, we'll
    want to ensure the stability of the system comes first.  This may mean
    immediately halting the transition to a well known, good state during
    mitigation.

### Log volume

We don't expect any performance issues due to logging, but will be monitoring
this closely during the migration. We will see an increase in the data being
captured that differs from our normal traffic patterns.  This includes the
following items:

* an increase in logging data for the `/-/readiness` check log messages, due to
  the reconfiguration of HAProxy and readiness requests in Kubernetes.
* The start and stopping of Pods will create a larger influx of messages related
  to those services' start up and shutdown events.  Currently we only see this
  from our VM's during mostly deployments.  However, we'll be rotating through
  more Pods during a deployment along with extra events from Pods that scale with
  our Auto-scaling mechanism
* Additional logs that weren't previously being captured from VM's.  The volume
  of this extra log data is relatively low, but may serve as useful and so we
  will keep these when transitioning into Kubernetes.  These extra logs include
  data from our database_load_balancing log, web_exporter, sidekiq_client,
  graphql, elasticsearch, audit, auth.  Feel free to surf these logs in our non
  prod environment to get a feel into what data is included in these logs.

### Prometheus metrics

Prometheus metrics are how we observe the behavor of our entire stack.  The
additional Pods will greatly increase the amount of metrics our systems will
need to process.  We will be monitoring Thanos, specifically the thanos-rule
process to ensure that we do not overwhelm our rule aggregations.  We monitor
this service heavily with its own set of SLO alerts.  Should any alerts trip
when we begin running this service in production, we'll need to stop and
evaluate how we can ease the pressure on our monitoring infrastructure.

## List the top operational risks when this feature goes live.

* Unexpected configuration differences between Cloud Native and Omnibus. We try
  to reduce the risk by auditing configuration in all environments.
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1470
  * The same audit has occurred when we first deployed the service into canary.
    Only one remaining discrepency remains which is considered a blocker towards
    going into production.
* We'll be switching from the Nginx service that runs on our VM's to the Nginx
  Ingress provided by our helm chart.
  * All differences will be accounted for prior to launch
    https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1683
  * We have a desire to investigate if it is possible to remove nginx, but this
    work will be completed after the service is fully running inside of
    Kubernetes prior.  This to be tracked:
    https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1557
  * We've removed nginx in our git service migrations as nginx wasn't doing any
    heavy lifting.  Keeping nginx in place will minimize the amount of change
    during the service migration between VM's and Kuberentes.
* Issues with HPA, not enough reserved capacity and saturation problems related
  to scaling
  * Monitoring HPA saturation can currently be done by hand, an issue to
    automate this and add this to our dashboards and alerting is proposed here:
    https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1417
* Disruption and user impact during upgrades due to long-lived connections
  * This has been evaluated and adjustments to our deployment configuration have
    been made to shorten deployments without any observed user impact.  See the
  results of this testing here:
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1558
* Issues with metrics overwhelming the current in-cluster Prometheus and
  ourThanos rule aggregation systems.
* GitLab-Runner relies heavily on the API being available.  Testing has shown
  that this service is very resilient with failures when speaking to the API
  endpoint.  More details and results from a test of gitlab-runner with some
  failure scenarios is noted here:
  https://gitlab.com/gitlab-com/gl-infra/readiness/-/merge_requests/72#note_554970121
* PGBouncer Saturation: We'll be monitoring pgbouncer closely to ensure that we
  are not unnecessarily saturating this service when making calls to our
  database.  Should we see connection saturation, this may delay the rollout of
  this service.  Saturation may come in the form of connected clients as the
  amount of running Pods will be far higher than the number of VM's.
* We lack visibility into the health and well-being of nginx:
  * https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1702
  * https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11974

## Security and Compliance

The security stature of this service does not change.  The scope of managing
security, however, will change as this is a shift in where the service is
running (Virtual Machines vs Kubernetes).  This service will receive security
updates in accordance to the [GitLab Release and Maintenance
Policy](https://docs.gitlab.com/ee/policy/maintenance.html)

### Configurations

Infrastructure changes and configurations are not lightweight and must go
through a rigorous review procedure and be vetted and tested prior to being
brought into our production environments.  Configurations that involve changes
that would modify our exposure or security posture, should also be reviewed by
our security team to ensure any proposal is appropriately vetted for any
potential risk.

If a proposed change contains unknown security concerns, there should be no
hesitation to rope in the security team to provide the needed confidence prior
to allowing such a change into our environments.


More details on reaching out to the appropriate team members can be found here:
https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/

### Application Upgrade and Rollback

The deployment mechanism for the API will mirror that of all other services
which utilize our own helm chart.  Please see our [existing Kubernetes
deployment documentation.](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/gitlab-com-deployer.md#kubernetes-upgrade)

The rails application uses the default BLACKOUT Period of 10 seconds.  Upon the
application receiving the first SIGTERM, the readiness probe will begin to fail
which pulls the Pod from receiving any future traffic.  We configure the Pod
terminationGracePeriodSeconds to 15.  This will allow any remaining requests
that are being serviced to complete prior to the Pod being forcibly removed from
the Infrastructure.  See issue
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1558 where we tuned
these values during testing.

### Cluster Upgrades

We are moving towards a more automated approach towards cluster upgrades to ensure
we stay on top of security improvements to the clusters that this services will
operate on.  More details of this can be found in
[delivery#1137](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1137)


## Performance

### Resource Configuration

Currently resources among all webservice deployments utilize the same
configuration.  This means no matter the workload style, the Pods that serve,
API, Git, and Websocket traffic, will all use the same Deployment Resource
Requests and Limits.  This is not a blocker going forward, but an issue exists
to resolve this: https://gitlab.com/gitlab-org/charts/gitlab/-/issues/2616

For the webservice container which runs Puma:

```yaml
resources:
  limits:
    memory: 6G
  requests:
    cpu: 4 (that's 4 total cpu cores, or 4000m)
    memory: 5G
```

For the workhorse container:

```yaml
resources:
  limits:
    memory: 2G
  requests:
    cpu: 600m (.6 cores)
    memory: 200M
```

The above was evaluated in issue:
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1592

It's been determined that we are over provisioning our CPU resources.  We can
counter this by an increase in puma workers.  This will require more testing to
find the right balance.


## Backup and Restore

This is a stateless component of GitLab.  No work to be done regarding backups
and restoration of customer data with this migration.

## Monitoring and Alerts

### Logging

We will mostly be using the same log indexes when running in Kubernetes, with
some small changes:

* All logs from the webservice container will be forwarded to the rails index.
  This includes both puma and rails, and all unstructured logs present in
`/var/log/gitlab/*`.
* All logs from the workhorse container will be forwarded to the workhorse index
* There are additional logs that will be forwarded to the ElasticSearch cluster
  that we are currently ignoring on VMs. The volume of these extra logs is low
and therefore do not believe this will cause too much additional load on the
cluster, and we have [disabled the unstructured production.log and
application.log](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/347).
* Logging coming from the rails pod is serviced by
  [GitLab-Logger](https://gitlab.com/gitlab-org/charts/components/gitlab-logger)

### Monitoring

We currently have a wide scope of monitoring that comes from the use of
`kube-state-metrics` as well as the exporter that is built into workhorse and
puma applications.  We'll continue to leverage our existing alerting rules to
warn us of problems related to the operational state of these Pods from the
scope of running inside of Kubernetes.

We also have a suite of metrics specifically monitoring the api backend in
haproxy which will alert us if we see excessive error rates or a drop in Apdex.

## Responsibility

SME's:

* Infrastructure:
  * Delivery
  * Scalability
  * Reliability
* Deployment: ~team::Delivery
* Helm Chart: ~team::Distribution
* API:
  * ~group::source code

## Testing

No testing was performed as this is a service that already exists in our
environment.  All testing has been circled around the migration of this service
into Kubernetes.  Refer to the associated epic for testing that was performed.
https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/271

Some highlights:

* Configuration auditing:
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1470
* Traffic behavior during deployments or Pod outages:
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1558
* Other blockers found during staging testing:
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1464

## Known issues going into production

* As discussed above, a blocking issue with Service Discovery:
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1592

## Readiness Reviewers

### Development:

* @stanhu

### Security:

* @ankelly
* @mlancini

### SRE's:

* @cmcfarland
* @cmiskell
* @marcel
