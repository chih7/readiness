# Kubernetes Deployments with Tanka

## Background

[Tanka](https://tanka.dev) is a tool for deploying Kubernetes resources. I'd
encourage those interested to read the website, but as a very brief summary:

- A framework for writing jsonnet that generates yaml kubernetes manifests
- Pipes those manifests into `kubectl diff|apply`. Clusters map 1:1 to tanka
  envs.
- Provides some kubernetes libsonnet libraries to simplify creation and
  manipulation of some common resources like Deployments.

## Current status

The observability has begun to use Tanka to deploy jaeger and thanos (their
respective operators, and CRD instances to be handled by those operators). We
use the monorepo
https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments.

We are starting to use tanka to deploy
[Woodhouse](https://gitlab.com/gitlab-com/gl-infra/woodhouse), the new SRE
tooling monolith.

## Design considerations and trade-offs

### Secrets

At the time of writing, tanka uses `kubectl diff` (with server-side diffs) to
compare resources on diff/apply/prune, and neither tanka nor kubectl suppress
secret output (https://github.com/kubernetes/kubernetes/issues/87840).

In order to avoid leaking secrets in tanka's output, we've decided to initially
configure secrets out of band. We manually create and edit secrets that are
referenced by tanka-managed resources.

In terms of manual work required, this is not hugely more burdensome than the
team's currently used secrets management methods. Most secrets must be edited by
a human at some point in the toolchain, regardless of how many layers of
indirection are in place.

#### Auditing

Currently, Kubernetes Secret objects are the only way we pass secrets to
applications - they don't currently fetch them at runtime from some other store.
Whether or not we derive these secrets from other resources (e.g. encrypted
files in a GCS bucket) or create them directly, it makes sense to build audit
infrastructure around kubernetes API events related to secrets. After all, SREs
do have direct access to the Kubernetes APIs and can subvert any system that
wraps that, if they wanted to.

GKE secret create and update events are automatically logged to GCP Stackdriver.

Example: [link](https://console.cloud.google.com/logs/viewer?project=gitlab-staging-1&minLogLevel=0&expandAll=false&timestamp=2020-10-08T11:45:05.200000000Z&customFacets=&limitCustomFacetWidth=true&advancedFilter=resource.type%3D%22k8s_cluster%22%0AprotoPayload.methodName%3D%22io.k8s.core.v1.secrets.update%22%0AprotoPayload.authenticationInfo.principalEmail%3D%22cfurman@gitlab.com%22&interval=PT1H&scrollTimestamp=2020-10-08T11:11:03.842574000Z&dateRangeStart=2020-10-08T10:45:09.451Z&dateRangeEnd=2020-10-08T11:45:09.451Z).

Example query:

```
resource.type="k8s_cluster"
protoPayload.methodName="io.k8s.core.v1.secrets.update"
```

It's outside the scope of this project to build infrastructure around these
audit events, as this is common to all Kubernetes workflows and orthogonal to
the use of Tanka.

### Distribution

Helm is the most popular distribution method for Kubernetes resources. While
jsonnet-bundler artifacts, which are simple to integrate with tanka, are
becoming more popular in certain domains (e.g.
https://github.com/coreos/kube-prometheus), there will be cases in which we
would like to avoid duplicating Helm-distributed resources. Forgoing helm has
the highest cost for resources that are low on customizability and high on
boilerplate, e.g. CRDs and Roles, typically distributed in helm charts for
operators (e.g. thanos and jaeger).

To get moving, we've copy-pasted such resources as yaml literals into the
tanka-deployments repo (see thanos and jaeger), importing them into jsonnet at
runtime so that they can be manipulated just like regular resources.

The current alpha releases of Tanka 0.12 include a helm chart ingestion feature,
eliminating the need for out of band importing of such resources. See
https://gitlab.com/-/snippets/2022014 for a quick spike on using these new
features.

### One `tk init` vs many

https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments, as it
stands now, contains one tanka structure created by `tk init` at the top level.
Tanka environments are used to represent different classes of software, with
deeper-nested environment to represent actual deployments of that software. For
example:

- environments/prometheus/gprd
- environments/prometheus/gstg
- environments/thanos/ops
- environments/thanos/dev

This gives us one jsonnet bundle and one lib directory for all tanka
deployments. Our own libsonnet must therefore be namespaced (see lib/thanos). We
can't use different versions of external dependencies for different deployments,
but honestly that might be worth it compared to the confusion of doing this.

### Dev/minikube environment

Tanka requires API server URLs be statically specified in each environment's
spec.json. Local kubernetes development environments such as minikube, k3d, and
KinD sometimes pick a random port at startup, leading to each user of a tanka
dev environment needing to create and ignore diffs in the spec.json. I am not
sure of a workaround for this.

## General thoughts on tanka

https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments is still
in its infancy, so these thoughts are relatively early and subject to change.

**Flexibility**

Tanka can be extremely simple. You can start by writing out full manifest
declarations in jsonnet, and abstract them into parameterised functions as and
when it makes sense. You can use or ignore the ksonnet library.

Writing jsonnet for tanka is analogous to writing a ruby script to serialise
manifests, with total control handed to the author.

**No moving parts**

Tanka generates manifests and applies them. `tk diff` uses `kubectl diff`, which
provides an accurate picture of what will happen to the cluster. Contrast this
with `helm diff`, which only diffs helm's own history, ignoring the actual state
of the cluster.

Tanka is stateless, and users of it are far less likely to end up in a situation
where upgrading the tool causes migration pain. If, for example, an upgrade to
the ksonnet library changes manifests in a way we don't like, we'll see it in
diffs and have total control over patching the generated objects to squash those
diffs.

**ksonnet**

Grafana have taken over maintenance of ksonnet. By default, tanka adds ksonnet
(https://github.com/ksonnet/ksonnet-lib) to your jsonnet-bundler dependencies
file, and somewhat encourages you to use it. It also adds
https://github.com/grafana/jsonnet-libs, which builds on ksonnet with some
higher-level abstractions.

This might say more about my jsonnet skills than anything else, but I quickly
found that it could be difficult to figure out what functions these libraries
provide, because the only way I can tell to do this is to open the
substantially-sized libsonnet files and grep through them. Then, you have the
problem of figuring out what behaviors are changed by the higher-level library's
patching.

The good news is that tanka's eval/show commands allow you to quickly figure out
what the overall effect of a library function is, and you're free to ignore as
much of ksonnet as you like.

## Why not helm?

### Specific problems with helm from the Observability team

One of the most important pieces of software we deploy with Helm is the
Prometheus operator. There are a few problems with this workflow:

**Lack of sharding support**

We will soon need to shard prometheus jobs in Kubernetes as we do in GCE, due to
the volume of metrics we ingest, differing sizes of indexes, and differing
access patterns. In GCE we currently run app (gitlab), DB, and default shards.

The prometheus-operator handles a Prometheus CRD. The prometheus-operator helm
chart deploys the operator, and exactly one Prometheus. This is a poor state of
affairs for sharding, and running multiple operators as a workaround is a
confusing option since they must be carefully configured to not pick up
Prometheus, ServiceMonitor, and Rule resources intended for a different shard.

The obvious workaround is to move the Prometheus declarations themselves into
one of our own helm charts. At this point, we'd be getting limited value from
the operator's helm chart, and writing more and more Helm ourselves. See the
"General problems with Helm" section for why I think this is not as good an
option as using Tanka.

Adding support for multiple Prometheus instances to the chart itself would need
to be either a breaking change or a very awkward non-breaking one.

A short discussion of sharding and shard migrations can be found here:
https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10542#note_380521348.

**Helm isn't necessarily the primary distribution channel**

As well as a manifest generation system, Helm is also a package manager for its
charts. This property is often stated as a reason to use helm: easy acquisition
of community charts.

The prometheus community seem to favour
https://github.com/coreos/kube-prometheus as a distribution method for
Prometheus software on Kubernetes - a jsonnet library, therefore importable by
tanka.

**Amount of extra helm required**

Parts of the interface to the prometheus-operator helm chart are references to
secrets injected as values. We write a helm chart to manage these resources, and
another to manage extra resources not provided by the prometheus-operator
community chart. We actually deploy [4
charts](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/releases/30-gitlab-monitoring/helmfile.yaml)
in total, 3 of which are written by us.

**Unknown upgrade path to helm 3**

To be fair, this is only unknown because we've not looked into it yet.
Difficulties upgrading chart releases to helm 3 are not uncommon though, and can
be comparable in effort to switching tool altogether.

### General problems with Helm

I'll try to keep this very brief since discussions on why Helm is bad are
common, but I'm happy to expand on it if that's wanted.

**Attribute mapping hell**

Since helm does not allow the chart user to manipulate the generated manifests,
many community helm charts expose a huge amount of the fields on every resource
as configurable values, effectively mapping entire resources in a piecemeal way.
The manifest templates become an unreadable sea of `{{  }}`.

**Templating**

Free text templating a configuration format with syntactically significant
whitespace is a pain that I think we're all familiar with.

## Prior work

This is mostly based on [the initial notes from a
spike](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11260),
and some initial thoughts on running some nascent deployments in
https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/.
