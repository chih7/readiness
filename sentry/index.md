## Summary

- [X] **Provide a high level summary of this new product feature. Explain how this change will benefit GitLab customers. Enumerate the customer use-cases.**

[Sentry](sentry.io) is currently used internally by developers and SREs at GitLab to track and analyse errors emitted by GitLab.com. 

The current [internal Sentry instance](https://sentry.gitlab.net) was first set up roughly 4 years ago on a single machine in our GCP `ops` environment. It is grossly outdated (and so missing features that would otherwise make its users' lives easier) and requires a lot of care and feeding from Infrastructure to keep it running.

Migrating to Sentry's SaaS offering was considered but ultimately not feasible: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/13963

Simply upgrading the existing instance would take a lot of effort and disrupt existing workflows. It would also not address the scaling issues that we currently have (all of its components are hosted on a single `n1-standard-64` machine).

Therefore we decided the best way to proceed would be to spin up a new Sentry instance hosted in Kubernetes and start sending events to it, while ramping down usage of the old Sentry instance. That work is tracked in this issue: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15372

This readiness review is for the [new Sentry instance](https://new-sentry.gitlab.net) running on Kubernetes which is now operational but not yet receiving any significant traffic.

- [X] **What metrics, including business metrics, should be monitored to ensure will this feature launch will be a success?**

Since this is an internal tool, to judge success we're mainly focusing on how it serves our existing use cases for Sentry, as well as how easily we can maintain, upgrade and scale it. 

## Architecture

- [X] **Add architecture diagrams to this issue of feature components and how they interact with existing GitLab components. Make sure to include the following: Internal dependencies, ports, encryption, protocols, security policies, etc.**

See [this page in the Sentry docs](https://develop.sentry.dev/architecture/) which the diagram below is _borrowed_ from.

```mermaid
graph TD
app["GitLab.com (Sentry SDKs)"] ==port 80==> lb{{Load Balancer}}
sentry_web ==port 5432==> postgres[("Cloud SQL (Postgres)")]
sentry_worker ==port 5432==> postgres
subgraph Sentry["Ops GKE cluster"]
    lb --> |"port 9000"| relay
    lb --> |"port 8080"| sentry_web["Sentry (web)"]
    relay --> |"port 9092"| kafka
    relay --> |"port 6379"| redis[(redis)]
    sentry_web --> |"port 1218"| snuba
    sentry_web --> |"port 11211"| memcached[(memcached)]
    sentry_web --> |"port 6379"| redis
    snuba --> kafka
    snuba --> |"port 6379"| redis
    snuba --> |"port 8123"| clickhouse[(clickhouse)]
    kafka --> |"port 2181"| zookeeper1[(zookeeper)]
    sentry_worker["Sentry (worker)"] --> |"port 11211"| memcached
    sentry_worker --> |"port 6379"| redis
    sentry_worker --> |"port 5672"| rabbitmq
    clickhouse --> |"port 2181"| zookeeper2[(zookeeper)]
end
```

Thick links indicate encrypted connections.

Also see [this diagram](https://getsentry.github.io/event-ingestion-graph/) which illustrates how an event is processed. 

- [X] **Describe each component of the new feature and enumerate what it does to support customer use cases.**

People accessing the [UI](https://new-sentry.gitlab.net) will be interacting with the GCP load balancer, `nginx` and `sentry-web`.

[Snuba](https://blog.sentry.io/2019/05/16/introducing-snuba-sentrys-new-search-infrastructure/) and Clickhouse (and their dependencies) power Sentry's event searching features.

Async processing is queued in RabbitMQ and processed by `sentry-worker` pods. I'm not 100% sure what is done async but imagine it might include sending emails, alerting, etc.

- [X] **For each component and dependency, what is the blast radius of failures? Is there anything in the feature design that will reduce this risk?**

See operational risk assessment section below for what gets affected when something fails.

Pods will be restarted automatically by Kubernetes if they crash - this is only expected to happen due to misconfigurations.

- [X] **If applicable, explain how this new feature will scale and any potential single points of failure in the design.**

I anticipate that in most cases, we can simply scale the cluster horizontally to process more events. We may need to allocate more resources to each component (storage, memory) if we find their current allocations insufficient. 

## Operational Risk Assessment

- [X] **What are the potential scalability or performance issues that may result with this change?**

As far as Sentry is concerned, only the scalability or performance of Sentry itself. There will be no effect on GitLab.com.

- [X] **Document every way the customer will interact with this new feature and how customers will be impacted by a failure of each interaction.**

Thankfully, sending events to Sentry is be non-blocking, so even if the new instance fails to process events, there is no discernible impact to the user. However, our developers will potentially lose visibility into user-impacting errors. 

- [X] **List the external and internal dependencies to the application (ex: redis, postgres, etc) for this feature and how the service will be impacted by a failure of that dependency.**

Event ingestion and search is affected if Relay, Snuba, Kafka, Zookeeper (either of them) or Clickhouse are in a degraded state. Failures here may be difficult to detect and might be surfaced with different symptoms, 

The Sentry application itself is affected if Postgres (Google Cloud SQL), Redis or Memcached are in a degraded state. I expect problems of this nature to be highly visible to people using the Sentry UI. 

- [X] **Were there any features cut or compromises made to make the feature launch?**

No but there is definitely room for improvement:

* We're still unsure if we can import projects and/or users from the existing Sentry instance as there has been a lot of config drift. 
* We plan to use Okta for authentication (but can't set it up until this readiness review is done!)
* Once the cluster is handling production traffic, we should set up alerting on specific application metrics (e.g. time to process events). We don't have a good baseline for this at the moment because of the low volume of traffic in staging.
* Certain parts of the cluster would be better off autoscaling on metrics such as queue size. We can't do this at the moment because of some missing infrastructure. [Issue here.](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16803)
* ~~Handling of incoming requests could be simplified by using an Nginx Ingress rather than Nginx behind a load balancer. [Issue here.](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16804)~~ This is done.

- [X] **List the top three operational risks when this feature goes live.**

The most obvious risk is scale. It's impossible to tell how well our current cluster configuration will handle all the events sent from GitLab.com, since we have no point of reference (the existing instance is very different in architecture). We can mitigate this somewhat by slowly introducing traffic. However, even if the cluster falls over once we start sending production traffic to it, there should be no effect on GitLab.com's customers, as sending events to Sentry should be non-blocking.

As we've never run Sentry in a Kubernetes cluster before, and there's limited documentation available on doing so, scaling characteristics and failure modes are a huge unknown for us. There are many moving parts, and limited/no support is provided by Sentry for people self-hosting the application in anything other than Docker Compose. I expect a fair bit of trial-and-error as we learn to administer the system. 

Since we haven't yet set up any alerting, the only way we would be able to tell there's something wrong with the cluster is by using it (or having a user report an issue to us). We should make setting up alerting a priority once production traffic starts hitting the cluster.

- [X] **What are a few operational concerns that will not be present at launch, but may be a concern later?**

Once the old instance is turned off, we will lose historical data that was stored on that instance ([this is expected](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15372#note_865201641) as no migration of past event data will be done). We need to ensure that both instances have the same views of _current_ data so that we don't have any observability blind spots.

- [X] **Can the new product feature be safely rolled back once it is live, can it be disabled using a feature flag?**

The change to start sending events to the new Sentry instance is fairly low risk at this point, as the old instance continues to operate. It is controlled by a feature flag `enable_new_sentry_integration` that is only usable after configuring the Sentry DSNs in the admin panel, and the rollout can be progressive or turned off altogther.

- [X] **As a thought experiment, think of worst-case failure scenarios for this product feature, how can the blast-radius of the failure be isolated?**

If, for whatever reason, sending events to Sentry does result in a degraded experience on GitLab.com, we can disable the feature flag for the new Sentry integration to stop the events. If this doesn't have the desired effect, we could even remove the instrumentation from the application altogether, although I find this highly unlikely to happen.

## Database

- [X] **If we use a database, is the data structure verified and vetted by the database team?**

The database schema is dictated by the application (Sentry).

- [X] **Do we have an approximate growth rate of the stored data (for capacity planning)?**

We do not have an approximate growth rate, but the existing Sentry instance's database is around 4.2TB after 4 years of heavy use.
```
postgres=# SELECT pg_size_pretty( pg_database_size('sentry') );
 pg_size_pretty
----------------
 4222 GB
(1 row)
```

- [X] **Can we age data and delete data of a certain age?**

The application provides a cleanup job runs every day and deletes event data older than 60 days (this is configurable).

## Security and Compliance

- **Are we adding any new resources of the following type? (If yes, please list them here or link to a place where they are listed)**
  - [X] **AWS Accounts/GCP Projects**
  - [X] **New Subnets**
  - [X] **VPC/Network Peering**
  - [X] **DNS names**
  - [X] **Entry-points exposed to the internet (Public IPs, Load-Balancers, Buckets, etc...)**
  - [X] **Other (anything relevant that might be worth mention)**

The cluster has been setup inside the existing ops GKE cluster, and so has similar workflows and compliance controls to other things using the same cluster.

We added a new DNS name `new-sentry.gitlab.net`. Access to the Sentry UI is protected by username/password authentication, with the option of adding the Okta integration later on. We also have the option of enforcing 2FA. 

Secrets used by the cluster are set in Vault and are versioned. The secrets are also stored in the 1Password Production vault for emergency access.

A new service account was added to the `ops` project in GCP. It is only assigned the roles it needs for the application to work: Cloud SQL user/client access, and Storage Object admin on a specific bucket in the `ops` project. Kubernetes service accounts used by the application's workloads are bound to this GCP service account. 

- **Secure Software Development Life Cycle (SSDLC)**
    - [X] **Is the configuration following a security standard? (CIS is a good baseline for example)**
    
* The ingress is configured to redirect all HTTP traffic to HTTPS.
* Access from the Internet is only available via the ingress. 
* The Postgres DB is only accessible via the [Cloud SQL proxy](https://cloud.google.com/sql/docs/postgres/sql-proxy). We further limit potential access by setting a [network policy](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/releases/sentry/ops-sql-proxy.yaml.gotmpl#L17) on it, such that only workloads in the correct namespace of the cluster can access the database.
* Authentication with Google is required to access the cluster (to make changes or view logs on running pods).
* 2FA is enforced for users.

    - [X] **All cloud infrastructure resources are labeled according to the [Infrastructure Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) guidelines**

Resources are under the `gitlab-ops` project in GCP, and the workloads are run in that project's GKE cluster. The Cloud SQL instance has been [labelled separately](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/4597).

    - [X] **Were the [GitLab security development guidelines](https://docs.gitlab.com/ee/development/secure_coding_guidelines.html) followed for this feature?**

Not applicable.

    - [X] **Do we have an automatic procedure to update the infrastructure (OS, container images, packages, etc...)**

Upgrades involve deploying a new version of the Helm chart and running migrations if necessary. The process is documented [here](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/sentry/sentry-in-kube.md).

    - [X] **Do we use IaC (Terraform) for all the infrastructure related to this feature? If not, what kind of resources are not covered?**

The underlying infrastructure is [provisioned using Terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/ops/sentry.tf), and the cluster itself is deployed using [Helm charts](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/sentry). 

        - [X] **Do we have secure static code analysis tools ([kics](https://github.com/Checkmarx/kics) or [checkov](https://github.com/bridgecrewio/checkov)) covering this feature's terraform?**

Interestingly the `config-mgmt` repo doesn't have any static code analysis running in its CI pipelines. Output of `checkov` on [`sentry.tf`](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/blob/master/environments/ops/sentry.tf):

```
       _               _
   ___| |__   ___  ___| | _______   __
  / __| '_ \ / _ \/ __| |/ / _ \ \ / /
 | (__| | | |  __/ (__|   < (_) \ V /
  \___|_| |_|\___|\___|_|\_\___/ \_/

By bridgecrew.io | version: 2.2.30
Update available 2.2.30 -> 2.2.39
Run pip3 install -U checkov to update


terraform scan results:

Passed checks: 10, Failed checks: 2, Skipped checks: 0

Check: CKV_GCP_29: "Ensure that Cloud Storage buckets have uniform bucket-level access enabled"
	PASSED for resource: google_storage_bucket.sentry-cluster-bucket
	File: /sentry.tf:28-41
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_gcs_2
Check: CKV_GCP_41: "Ensure that IAM users are not assigned the Service Account User or Service Account Token Creator roles at project level"
	PASSED for resource: google_project_iam_member.sentry-k8s-sa-cloud-sql-client
	File: /sentry.tf:50-54
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_iam_3
Check: CKV_GCP_42: "Ensure that Service Account has no Admin privileges"
	PASSED for resource: google_project_iam_member.sentry-k8s-sa-cloud-sql-client
	File: /sentry.tf:50-54
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_iam_4
Check: CKV_GCP_49: "Ensure roles do not impersonate or manage Service Accounts used at project level"
	PASSED for resource: google_project_iam_member.sentry-k8s-sa-cloud-sql-client
	File: /sentry.tf:50-54
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_iam_10
Check: CKV_GCP_46: "Ensure Default Service account is not used at a project level"
	PASSED for resource: google_project_iam_member.sentry-k8s-sa-cloud-sql-client
	File: /sentry.tf:50-54
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_iam_7
Check: CKV_GCP_41: "Ensure that IAM users are not assigned the Service Account User or Service Account Token Creator roles at project level"
	PASSED for resource: google_project_iam_member.sentry-k8s-sa-cloud-sql-user
	File: /sentry.tf:56-60
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_iam_3
Check: CKV_GCP_42: "Ensure that Service Account has no Admin privileges"
	PASSED for resource: google_project_iam_member.sentry-k8s-sa-cloud-sql-user
	File: /sentry.tf:56-60
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_iam_4
Check: CKV_GCP_49: "Ensure roles do not impersonate or manage Service Accounts used at project level"
	PASSED for resource: google_project_iam_member.sentry-k8s-sa-cloud-sql-user
	File: /sentry.tf:56-60
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_iam_10
Check: CKV_GCP_46: "Ensure Default Service account is not used at a project level"
	PASSED for resource: google_project_iam_member.sentry-k8s-sa-cloud-sql-user
	File: /sentry.tf:56-60
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_iam_7
Check: CKV_GCP_28: "Ensure that Cloud Storage bucket is not anonymously or publicly accessible"
	PASSED for resource: google_storage_bucket_iam_member.sentry-k8s-sa-storage
	File: /sentry.tf:69-73
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_public_1
Check: CKV_GCP_62: "Bucket should log access"
	FAILED for resource: google_storage_bucket.sentry-cluster-bucket
	File: /sentry.tf:28-41
	Guide: https://docs.bridgecrew.io/docs/bc_gcp_logging_2

		28 | resource "google_storage_bucket" "sentry-cluster-bucket" {
		29 |   name                        = "sentry-cluster-bucket"
		30 |   location                    = "US"
		31 |   uniform_bucket_level_access = true
		32 |
		33 |   versioning {
		34 |     enabled = "false"
		35 |   }
		36 |
		37 |   labels = {
		38 |     tfmanaged = "yes"
		39 |     name      = "sentry-cluster-bucket"
		40 |   }
		41 | }

Check: CKV_GCP_78: "Ensure Cloud storage has versioning enabled"
	FAILED for resource: google_storage_bucket.sentry-cluster-bucket
	File: /sentry.tf:28-41
	Guide: https://docs.bridgecrew.io/docs/ensure-gcp-cloud-storage-has-versioning-enabled

		28 | resource "google_storage_bucket" "sentry-cluster-bucket" {
		29 |   name                        = "sentry-cluster-bucket"
		30 |   location                    = "US"
		31 |   uniform_bucket_level_access = true
		32 |
		33 |   versioning {
		34 |     enabled = "false"
		35 |   }
		36 |
		37 |   labels = {
		38 |     tfmanaged = "yes"
		39 |     name      = "sentry-cluster-bucket"
		40 |   }
		41 | }
```

* There is no need to log access to the bucket or use versioning since it's only used by the Sentry application itself. We don't expect people to access things in the bucket direectly.

  - **If there's a new terraform state:**
    - [X] **Where is to terraform state stored, and who has access to it?**
    - [X] **Does this feature add secrets to the terraform state? If yes, can they be stored in a secrets manager?**

Not applicable.

  - **If we're creating new containers:**
    - [X] **Are we using a distroless base image?**
    - **Do we have security scanners covering these containers?**
      - [X] **`kics` or `checkov` for Dockerfiles for example**
      - [X] **[GitLab's container](https://docs.gitlab.com/ee/user/application_security/container_scanning/#configuration) scanner for vulnerabilities**

Not applicable.

- **Identity and Access Management**
  -  [X] Are we adding any new forms of **Authentication** (New service-accounts, users/password for storage, OIDC, etc...)?
  -  [X] **Does it follow the least privilege principle?**

Yes. Sentry has its own authentication, but we plan to use the Okta integration later. All new users are given the lowest level of access (member) on joining, and 2FA is enforced for all users.

- **If we are adding any new Data Storage (Databases, buckets, etc...)**
  -  [X] **What kind of data is stored on each system? (secrets, customer data, audit, etc...)**

Postgres is the main datastore for the Sentry application. Users, preferences, [raw unprocessed event data](https://forum.sentry.io/t/postgres-nodestore-node-table-124gb/12753/3), comments, etc., are stored in Postgres. 

Aggregated event data is stored in Clickhouse.

  -  [X] **How is data rated according to our [data classification standard](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html) (customer data is RED)**

The existing Sentry instance is rated **YELLOW**, so the new instance would be the same.

  -  [X] **Is data it encrypted at rest? (If the storage is provided by a GCP service, the answer is most likely yes)**

Yes, storage is provided by Cloud SQL.

  -  [X] **Do we have audit logs on data access?**

We do not have [Data Access audit logs](https://cloud.google.com/logging/docs/audit#data-access) enabled, and due to the amount of data ingested, it would probably be highly uneconomical. Sentry has its [own audit logging functionality](https://new-sentry.gitlab.net/settings/gitlab/audit-log/) but it only tracks changes to the instance or projects. 

- **Network security (encryption and ports should be clear in the architecture diagram above)**
  -  [X] **Firewalls follow the least privilege principle (w/ network policies in Kubernetes or firewalls on cloud provider)**

A network policy restricts access to the Postgres database to the `sentry` namespace of the ops GKE cluster only. 

  -  [X] **Is the service covered by any DDoS protection solution (GCP/AWS load-balancers or Cloudflare usually cover this)**

Yes, the application is proxied via Cloudflare.

  -  [X] **Is the service covered by a WAF (Web Application Firewall)**

Yes, the application is proxied via Cloudflare.

- **Logging & Audit**
  - [X] **Has effort been made to obscure or elide sensitive customer data in logging?**

Sensitive data (such as PII) in events should be [scrubbed by the clientside SDK](https://docs.sentry.io/platforms/javascript/data-management/sensitive-data/). There are also [serverside scrubbing options](https://docs.sentry.io/product/data-management-settings/scrubbing/server-side-scrubbing/) we can make use of. 

- **Compliance**
    - [X] **Is the service subject to any regulatory/compliance standards? If so, detail which and provide details on applicable controls, management processes, additional monitoring, and mitigating factors.**

Access to Sentry is expected to be limited to the existing cohort of users authorised for the current instance. I am not aware of any additional regulatory/compliance controls for Sentry. I expect them to be the same as what's in place for the existing instance.

## Performance

- [X] **Explain what validation was done following GitLab's [performance guidelines](https://docs.gitlab.com/ee/development/performance.html). Please explain which tools were used and link to the results below.**

Not really applicable. We're currently sending some staging traffic to it to validate that the cluster behaves, and will later introduce production traffic.

- [X] **Are there any potential performance impacts on the database when this feature is enabled at GitLab.com scale?**

No. 

- [X] **Are there any throttling limits imposed by this feature? If so how are they managed?**
- [X] **If there are throttling limits, what is the customer experience of hitting a limit?**

There are [serverside rate limits](https://sentry.gitlab.net/settings/gitlab/rate-limits/) in Sentry that we're [currently hitting](https://sentry.gitlab.net/organizations/gitlab/stats/) on the existing instance. Rate limiting seems to have changed in newer versions of Sentry so I'm unsure how it will behave. I have disabled rate limiting on the new instance for now. _If_ we do hit a rate limit, there will be no effect on users of GitLab.com, but our visibility of user-impacting errors might suffer. The Sentry SDKs are also expected to [respect rate limit responses](https://develop.sentry.dev/sdk/rate-limiting/) from the server.

There should be no performance impact on GitLab.com's users when we start sending production events to the new Sentry cluster.

- [X] **For all dependencies external and internal to the application, are there retry and back-off strategies for them?**

In addition to whatever is supported by the Sentry SDKs, I expect the Sentry application itself has retry/backoff logic baked in. 

- [X] **Does the feature account for brief spikes in traffic, at least 2x above the expected TPS?**

We're currently [investigating how well the cluster scales](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16717#note_1153738037) with traffic from staging. However, as there are far more events being sent in production, we would still need to slowly ramp up the number of production events being sent to the new Sentry instance. In any case, certain components of the cluster are able to autoscale based on resource (CPU/memory) pressure.
## Backup and Restore

- [X] **Outside of existing backups, are there any other customer data that needs to be backed up for this product feature?**
- [X] **Are backups monitored?**
- [X] **Was a restore from backup tested?**

Backups of the Postgres database are done every day and retained for 7 days via Cloud SQL. No backups are done of the 

No customer data is stored in Sentry so no additional backups are needed.

We have not tested restoring backups - the amount of data currently available in Sentry makes a meaningful test difficult to carry out.

## Monitoring and Alerts

- [X] **Is the service logging in JSON format and are logs forwarded to logstash?**

All cluster logs are forwarded to Elasticsearch and searchable in Kibana.

- [X] **Is the service reporting metrics to Prometheus?**

Yes. Everything in the cluster that supports exporting metrics is reporting. However, at time of writing, Clickhouse metrics are not available (but I expect to have this fixed shortly).

- [X] **How is the end-to-end customer experience measured?**
- [X] **Do we have a target SLA in place for this service?**
- [X] **Do we know what the indicators (SLI) are that map to the target SLA?**
- [X] **Do we have alerts that are triggered when the SLI's (and thus the SLA) are not met?**
- [X] **Do we have troubleshooting runbooks linked to these alerts?**

As this is a tool for internal use there are no SLAs or SLIs in place. Support is expected to be best effort only.

Sentry helpfully provides an in-application measurement of Apdex, which should suffice for monitoring the quality of the overall experience. We don't have alerts or runbooks for this yet since we have a limited understanding of the ideal state (under production load) and what causes Apdex regression in Sentry.

- [X] **What are the thresholds for tweeting or issuing an official customer notification for an outage related to this feature?**

Sentry is not accessible to customers.

- [X] **do the oncall rotations responsible for this service have access to this service?**

They will once we're able to integrate with Okta.

## Responsibility

- [X] **Which individuals are the subject matter experts and know the most about this feature?**
- [X] **Which team or set of individuals will take responsibility for the reliability of the feature once it is in production?**
- [X] **Is someone from the team who built the feature on call for the launch? If not, why not?**

@ayeung is most familiar with this particular Sentry instance so should be the first point of contact. The Infrastructure team are nominally responsible for the care and feeding of both the old and new Sentry instances, and will support it during on-call. 

In the future it may be desirable for the heaviest users of Sentry (developer teams and/or support) to be able to self-serve the administration of Sentry, but there are no concrete plans around this yet.

## Testing

- [X] **Describe the load test plan used for this feature. What breaking points were validated?**
- [X] **For the component failures that were theorized for this feature, were they tested? If so include the results of these failure tests.**

Initial validation testing is being carried out in [this issue](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15372). Staging events are being sent to the instance in an effort to determine its scaling characteristics.

- [X] **Give a brief overview of what tests are run automatically in GitLab's CI/CD pipeline for this feature?**

As this is an application provided by a third-party vendor, there are no appropriate tests that can be run in CI/CD for it.
